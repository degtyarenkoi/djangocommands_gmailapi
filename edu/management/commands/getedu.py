from django.core.management.base import BaseCommand,  LabelCommand
from edu.models import Document
from django.contrib.auth.models import User

class Command(LabelCommand):
    def handle(self, *args, **options):
        help = 'Allows to look through a list of people that have specified education'
        lst = []
        for arg in args:
            docs = Document.objects.filter(education=arg)
            lst.append(docs)
        for l in lst:
            for i in l:
                people = User.objects.get(id=i.people_id)
                self.stdout.write(people.username, ending='\n')