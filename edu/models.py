from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Document(models.Model):
    class Meta():
        db_table = 'document'

    education = models.CharField(max_length=70)
    people = models.ForeignKey(User)