from django.shortcuts import render_to_response, redirect, render
from models import Document
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt

from gmail_api import get_messages

# Create your views here.

def main(request):
    docs = Document.objects.all()
    users = User.objects.all()
    mess = get_messages()
    return render_to_response('tmp.html', {'title':'edu of ppl', 'users':users, 'docs':docs, 'mess':mess})

@csrf_exempt
def add_person(request):
    if request.method == 'POST':
        name = request.POST['name']
        user = User.objects.create(username=name)
    return redirect('/')

def del_person(request, user_id):
    if request.method == 'GET':
        user = User.objects.get(id=user_id)
        user.delete()
    return redirect('/')

@csrf_exempt
def create_edu(request):
    if request.method == 'POST':
        edu = request.POST['edu']
        person = request.POST['person']
        user = User.objects.get(username=person)
        Document.objects.create(education=edu, people_id=user.id)
    return redirect('/')

def del_doc(request, doc_id):
    if request.method == 'GET':
        doc = Document.objects.get(id=doc_id)
        doc.delete()
    return redirect('/')